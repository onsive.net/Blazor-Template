﻿using OnSive.Web.Shared.Settings;
using OnSive.Web.Shared.Wrapper;
using System.Threading.Tasks;

namespace OnSive.Web.Shared.Managers
{
    public interface IPreferenceManager
    {
        Task SetPreference(IPreference preference);

        Task<IPreference> GetPreference();

        Task<IResult> ChangeLanguageAsync(string languageCode);
    }
}