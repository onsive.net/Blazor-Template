﻿namespace OnSive.Web.Shared.Settings
{
    public interface IPreference
    {
        public string LanguageCode { get; set; }
    }
}
