﻿using AutoMapper;
using OnSive.Web.Application.Requests.Identity;
using OnSive.Web.Application.Responses.Identity;

namespace OnSive.Web.Infrastructure.Client.Mappings
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<PermissionResponse, PermissionRequest>().ReverseMap();
            CreateMap<RoleClaimResponse, RoleClaimRequest>().ReverseMap();
        }
    }
}