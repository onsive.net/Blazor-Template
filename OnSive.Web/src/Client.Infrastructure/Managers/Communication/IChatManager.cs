﻿using OnSive.Web.Application.Interfaces.Chat;
using OnSive.Web.Application.Models.Chat;
using OnSive.Web.Application.Responses.Identity;
using OnSive.Web.Infrastructure.Client.Managers;
using OnSive.Web.Shared.Wrapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Communication
{
    public interface IChatManager : IManager
    {
        Task<IResult<IEnumerable<ChatUserResponse>>> GetChatUsersAsync();

        Task<IResult> SaveMessageAsync(ChatHistory<IChatUser> chatHistory);

        Task<IResult<IEnumerable<ChatHistoryResponse>>> GetChatHistoryAsync(string cId);
    }
}