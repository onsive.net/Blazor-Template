﻿using OnSive.Web.Application.Requests.Identity;
using OnSive.Web.Application.Responses.Identity;
using OnSive.Web.Shared.Wrapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Identity.Roles
{
    public interface IRoleManager : IManager
    {
        Task<IResult<List<RoleResponse>>> GetRolesAsync();

        Task<IResult<string>> SaveAsync(RoleRequest role);

        Task<IResult<string>> DeleteAsync(string id);

        Task<IResult<PermissionResponse>> GetPermissionsAsync(string roleId);

        Task<IResult<string>> UpdatePermissionsAsync(PermissionRequest request);
    }
}