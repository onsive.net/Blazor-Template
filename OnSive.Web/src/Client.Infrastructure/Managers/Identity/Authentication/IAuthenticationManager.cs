﻿using OnSive.Web.Application.Requests.Identity;
using OnSive.Web.Infrastructure.Client.Managers;
using OnSive.Web.Shared.Wrapper;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Identity.Authentication
{
    public interface IAuthenticationManager : IManager
    {
        Task<IResult> Login(TokenRequest model);

        Task<IResult> Logout();

        Task<string> RefreshToken();

        Task<string> TryRefreshToken();

        Task<string> TryForceRefreshToken();

        Task<ClaimsPrincipal> CurrentUser();
    }
}