﻿using System.Threading.Tasks;
using Toolbelt.Blazor;

namespace OnSive.Web.Infrastructure.Client.Managers.Interceptors
{
    public interface IHttpInterceptorManager : IManager
    {
        void RegisterEvent();

        Task InterceptBeforeHttpAsync(object sender, HttpClientInterceptorEventArgs e);

        void DisposeEvent();
    }
}