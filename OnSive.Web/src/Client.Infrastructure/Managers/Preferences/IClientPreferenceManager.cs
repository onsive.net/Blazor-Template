﻿using MudBlazor;
using OnSive.Web.Shared.Managers;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Preferences
{
    public interface IClientPreferenceManager : IPreferenceManager
    {
        Task<MudTheme> GetCurrentThemeAsync();

        Task<bool> ToggleDarkModeAsync();
    }
}