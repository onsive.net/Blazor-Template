﻿using OnSive.Web.Application.Features.Dashboards.Queries.GetData;
using OnSive.Web.Infrastructure.Client.Managers;
using OnSive.Web.Shared.Wrapper;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Dashboard
{
    public interface IDashboardManager : IManager
    {
        Task<IResult<DashboardDataResponse>> GetDataAsync();
    }
}