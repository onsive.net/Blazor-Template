﻿using OnSive.Web.Application.Features.Dashboards.Queries.GetData;
using OnSive.Web.Infrastructure.Client.Extensions;
using OnSive.Web.Shared.Wrapper;
using System.Net.Http;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Dashboard
{
    public class DashboardManager : IDashboardManager
    {
        private readonly HttpClient _httpClient;

        public DashboardManager(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IResult<DashboardDataResponse>> GetDataAsync()
        {
            var response = await _httpClient.GetAsync(Routes.DashboardEndpoints.GetData);
            var data = await response.ToResult<DashboardDataResponse>();
            return data;
        }
    }
}