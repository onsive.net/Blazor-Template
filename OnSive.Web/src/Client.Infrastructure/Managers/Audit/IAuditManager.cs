﻿using OnSive.Web.Application.Responses.Audit;
using OnSive.Web.Infrastructure.Client.Managers;
using OnSive.Web.Shared.Wrapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Audit
{
    public interface IAuditManager : IManager
    {
        Task<IResult<IEnumerable<AuditResponse>>> GetCurrentUserTrailsAsync();

        Task<IResult<string>> DownloadFileAsync(string searchString = "", bool searchInOldValues = false, bool searchInNewValues = false);
    }
}