﻿using OnSive.Web.Application.Features.Documents.Commands.AddEdit;
using OnSive.Web.Application.Features.Documents.Queries.GetAll;
using OnSive.Web.Application.Features.Documents.Queries.GetById;
using OnSive.Web.Application.Requests.Documents;
using OnSive.Web.Shared.Wrapper;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Misc.Document
{
    public interface IDocumentManager : IManager
    {
        Task<PaginatedResult<GetAllDocumentsResponse>> GetAllAsync(GetAllPagedDocumentsRequest request);

        Task<IResult<GetDocumentByIdResponse>> GetByIdAsync(GetDocumentByIdQuery request);

        Task<IResult<int>> SaveAsync(AddEditDocumentCommand request);

        Task<IResult<int>> DeleteAsync(int id);
    }
}