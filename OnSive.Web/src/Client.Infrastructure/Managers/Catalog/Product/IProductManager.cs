﻿using OnSive.Web.Application.Features.Products.Commands.AddEdit;
using OnSive.Web.Application.Features.Products.Queries.GetAllPaged;
using OnSive.Web.Application.Requests.Catalog;
using OnSive.Web.Shared.Wrapper;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Client.Managers.Catalog.Product
{
    public interface IProductManager : IManager
    {
        Task<PaginatedResult<GetAllPagedProductsResponse>> GetProductsAsync(GetAllPagedProductsRequest request);

        Task<IResult<string>> GetProductImageAsync(int id);

        Task<IResult<int>> SaveAsync(AddEditProductCommand request);

        Task<IResult<int>> DeleteAsync(int id);

        Task<IResult<string>> ExportToExcelAsync(string searchString = "");
    }
}