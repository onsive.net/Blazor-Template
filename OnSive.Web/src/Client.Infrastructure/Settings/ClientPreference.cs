﻿using OnSive.Web.Shared.Constants.Localization;
using OnSive.Web.Shared.Settings;
using System.Linq;

namespace OnSive.Web.Infrastructure.Client.Settings
{
    public record ClientPreference : IPreference
    {
        public bool IsDarkMode { get; set; }
        public bool IsRTL { get; set; }
        public bool IsDrawerOpen { get; set; }
        public string PrimaryColor { get; set; }
        public string LanguageCode { get; set; } = LocalizationConstants.SupportedLanguages.FirstOrDefault()?.Code ?? "en-US";
    }
}