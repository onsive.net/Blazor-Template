﻿namespace OnSive.Web.Infrastructure.Client.Routes
{
    public class DashboardEndpoints
    {
        public static string GetData = "api/v1/dashboard";
    }
}