﻿using OnSive.Web.Domain.Contracts;

namespace OnSive.Web.Domain.Entities.Misc
{
    public class DocumentType : AuditableEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}