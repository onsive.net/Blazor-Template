﻿using OnSive.Web.Domain.Contracts;
using OnSive.Web.Domain.Entities.Misc;

namespace OnSive.Web.Domain.Entities.ExtendedAttributes
{
    public class DocumentExtendedAttribute : AuditableEntityExtendedAttribute<int, int, Document>
    {
    }
}