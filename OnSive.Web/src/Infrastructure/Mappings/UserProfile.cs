﻿using AutoMapper;
using OnSive.Web.Application.Responses.Identity;
using OnSive.Web.Infrastructure.Server.Models.Identity;

namespace OnSive.Web.Infrastructure.Server.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserResponse, BlazorHeroUser>().ReverseMap();
            CreateMap<ChatUserResponse, BlazorHeroUser>().ReverseMap()
                .ForMember(dest => dest.EmailAddress, source => source.MapFrom(source => source.Email)); //Specific Mapping
        }
    }
}