﻿using AutoMapper;
using OnSive.Web.Application.Interfaces.Chat;
using OnSive.Web.Application.Models.Chat;
using OnSive.Web.Infrastructure.Server.Models.Identity;

namespace OnSive.Web.Infrastructure.Server.Mappings
{
    public class ChatHistoryProfile : Profile
    {
        public ChatHistoryProfile()
        {
            CreateMap<ChatHistory<IChatUser>, ChatHistory<BlazorHeroUser>>().ReverseMap();
        }
    }
}