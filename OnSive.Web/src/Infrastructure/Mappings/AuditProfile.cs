﻿using AutoMapper;
using OnSive.Web.Application.Responses.Audit;
using OnSive.Web.Infrastructure.Server.Models.Audit;

namespace OnSive.Web.Infrastructure.Server.Mappings
{
    public class AuditProfile : Profile
    {
        public AuditProfile()
        {
            CreateMap<AuditResponse, Audit>().ReverseMap();
        }
    }
}