﻿using AutoMapper;
using OnSive.Web.Application.Responses.Identity;
using OnSive.Web.Infrastructure.Server.Models.Identity;

namespace OnSive.Web.Infrastructure.Server.Mappings
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<RoleResponse, BlazorHeroRole>().ReverseMap();
        }
    }
}