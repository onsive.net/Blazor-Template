﻿using Microsoft.EntityFrameworkCore;
using OnSive.Web.Application.Interfaces.Repositories;
using OnSive.Web.Domain.Entities.Catalog;
using System.Threading.Tasks;

namespace OnSive.Web.Infrastructure.Server.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly IRepositoryAsync<Product, int> _repository;

        public ProductRepository(IRepositoryAsync<Product, int> repository)
        {
            _repository = repository;
        }

        public async Task<bool> IsBrandUsed(int brandId)
        {
            return await _repository.Entities.AnyAsync(b => b.BrandId == brandId);
        }
    }
}