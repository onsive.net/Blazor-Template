﻿using OnSive.Web.Application.Interfaces.Repositories;
using OnSive.Web.Domain.Entities.Catalog;

namespace OnSive.Web.Infrastructure.Server.Repositories
{
    public class BrandRepository : IBrandRepository
    {
        private readonly IRepositoryAsync<Brand, int> _repository;

        public BrandRepository(IRepositoryAsync<Brand, int> repository)
        {
            _repository = repository;
        }
    }
}