﻿using OnSive.Web.Application.Interfaces.Repositories;
using OnSive.Web.Domain.Entities.Misc;

namespace OnSive.Web.Infrastructure.Server.Repositories
{
    public class DocumentTypeRepository : IDocumentTypeRepository
    {
        private readonly IRepositoryAsync<DocumentType, int> _repository;

        public DocumentTypeRepository(IRepositoryAsync<DocumentType, int> repository)
        {
            _repository = repository;
        }
    }
}