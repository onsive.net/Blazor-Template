﻿using Microsoft.Extensions.DependencyInjection;
using OnSive.Web.Application.Interfaces.Repositories;
using OnSive.Web.Application.Interfaces.Serialization.Serializers;
using OnSive.Web.Application.Interfaces.Services.Storage;
using OnSive.Web.Application.Interfaces.Services.Storage.Provider;
using OnSive.Web.Application.Serialization.JsonConverters;
using OnSive.Web.Application.Serialization.Options;
using OnSive.Web.Application.Serialization.Serializers;
using OnSive.Web.Infrastructure.Server.Repositories;
using OnSive.Web.Infrastructure.Server.Services.Storage;
using OnSive.Web.Infrastructure.Server.Services.Storage.Provider;
using System;
using System.Linq;
using System.Reflection;

namespace OnSive.Web.Infrastructure.Server.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddInfrastructureMappings(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            return services
                .AddTransient(typeof(IRepositoryAsync<,>), typeof(RepositoryAsync<,>))
                .AddTransient<IProductRepository, ProductRepository>()
                .AddTransient<IBrandRepository, BrandRepository>()
                .AddTransient<IDocumentRepository, DocumentRepository>()
                .AddTransient<IDocumentTypeRepository, DocumentTypeRepository>()
                .AddTransient(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));
        }

        public static IServiceCollection AddExtendedAttributesUnitOfWork(this IServiceCollection services)
        {
            return services
                .AddTransient(typeof(IExtendedAttributeUnitOfWork<,,>), typeof(ExtendedAttributeUnitOfWork<,,>));
        }

        public static IServiceCollection AddServerStorage(this IServiceCollection services)
            => services.AddServerStorage(null);

        public static IServiceCollection AddServerStorage(this IServiceCollection services, Action<SystemTextJsonOptions> configure)
        {
            return services
                .AddScoped<IJsonSerializer, SystemTextJsonSerializer>()
                .AddScoped<IStorageProvider, ServerStorageProvider>()
                .AddScoped<IServerStorageService, ServerStorageService>()
                .AddScoped<ISyncServerStorageService, ServerStorageService>()
                .Configure<SystemTextJsonOptions>(configureOptions =>
                {
                    configure?.Invoke(configureOptions);
                    if (!configureOptions.JsonSerializerOptions.Converters.Any(c => c.GetType() == typeof(TimespanJsonConverter)))
                        configureOptions.JsonSerializerOptions.Converters.Add(new TimespanJsonConverter());
                });
        }
    }
}