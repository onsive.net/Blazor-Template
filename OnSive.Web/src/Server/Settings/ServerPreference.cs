﻿using OnSive.Web.Shared.Constants.Localization;
using OnSive.Web.Shared.Settings;
using System.Linq;

namespace OnSive.Web.Server.Settings
{
    public record ServerPreference : IPreference
    {
        public string LanguageCode { get; set; } = LocalizationConstants.SupportedLanguages.FirstOrDefault()?.Code ?? "en-US";

        //TODO - add server preferences
    }
}