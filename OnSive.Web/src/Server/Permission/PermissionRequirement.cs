﻿using Microsoft.AspNetCore.Authorization;

namespace OnSive.Web.Server.Permission
{
    internal class PermissionRequirement : IAuthorizationRequirement
    {
        public string Permission { get; private set; }

        public PermissionRequirement(string permission)
        {
            Permission = permission;
        }
    }
}