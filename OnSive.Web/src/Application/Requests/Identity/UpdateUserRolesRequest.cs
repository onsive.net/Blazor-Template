﻿using OnSive.Web.Application.Responses.Identity;
using System.Collections.Generic;

namespace OnSive.Web.Application.Requests.Identity
{
    public class UpdateUserRolesRequest
    {
        public string UserId { get; set; }
        public IList<UserRoleModel> UserRoles { get; set; }
    }
}