﻿using System.ComponentModel.DataAnnotations;

namespace OnSive.Web.Application.Requests.Identity
{
    public class ForgotPasswordRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}