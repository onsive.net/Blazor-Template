﻿using System.Collections.Generic;

namespace OnSive.Web.Application.Responses.Identity
{
    public class GetAllUsersResponse
    {
        public IEnumerable<UserResponse> Users { get; set; }
    }
}