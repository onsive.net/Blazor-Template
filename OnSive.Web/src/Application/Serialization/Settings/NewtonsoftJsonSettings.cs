﻿
using Newtonsoft.Json;
using OnSive.Web.Application.Interfaces.Serialization.Settings;

namespace OnSive.Web.Application.Serialization.Settings
{
    public class NewtonsoftJsonSettings : IJsonSerializerSettings
    {
        public JsonSerializerSettings JsonSerializerSettings { get; } = new();
    }
}