﻿using OnSive.Web.Application.Interfaces.Serialization.Options;
using System.Text.Json;

namespace OnSive.Web.Application.Serialization.Options
{
    public class SystemTextJsonOptions : IJsonSerializerOptions
    {
        public JsonSerializerOptions JsonSerializerOptions { get; } = new();
    }
}