﻿using OnSive.Web.Application.Requests;

namespace OnSive.Web.Application.Interfaces.Services
{
    public interface IUploadService
    {
        string UploadAsync(UploadRequest request);
    }
}