﻿using OnSive.Web.Application.Interfaces.Common;
using OnSive.Web.Application.Requests.Identity;
using OnSive.Web.Shared.Wrapper;
using System.Threading.Tasks;

namespace OnSive.Web.Application.Interfaces.Services.Account
{
    public interface IAccountService : IService
    {
        Task<IResult> UpdateProfileAsync(UpdateProfileRequest model, string userId);

        Task<IResult> ChangePasswordAsync(ChangePasswordRequest model, string userId);

        Task<IResult<string>> GetProfilePictureAsync(string userId);

        Task<IResult<string>> UpdateProfilePictureAsync(UpdateProfilePictureRequest request, string userId);
    }
}