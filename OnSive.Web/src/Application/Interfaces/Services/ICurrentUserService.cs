﻿using OnSive.Web.Application.Interfaces.Common;

namespace OnSive.Web.Application.Interfaces.Services
{
    public interface ICurrentUserService : IService
    {
        string UserId { get; }
    }
}