﻿using OnSive.Web.Application.Requests.Mail;
using System.Threading.Tasks;

namespace OnSive.Web.Application.Interfaces.Services
{
    public interface IMailService
    {
        Task SendAsync(MailRequest request);
    }
}