﻿using OnSive.Web.Application.Interfaces.Common;
using OnSive.Web.Application.Requests.Identity;
using OnSive.Web.Application.Responses.Identity;
using OnSive.Web.Shared.Wrapper;
using System.Threading.Tasks;

namespace OnSive.Web.Application.Interfaces.Services.Identity
{
    public interface ITokenService : IService
    {
        Task<Result<TokenResponse>> LoginAsync(TokenRequest model);

        Task<Result<TokenResponse>> GetRefreshTokenAsync(RefreshTokenRequest model);
    }
}