﻿using OnSive.Web.Application.Interfaces.Chat;
using OnSive.Web.Application.Models.Chat;
using OnSive.Web.Application.Responses.Identity;
using OnSive.Web.Shared.Wrapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnSive.Web.Application.Interfaces.Services
{
    public interface IChatService
    {
        Task<Result<IEnumerable<ChatUserResponse>>> GetChatUsersAsync(string userId);

        Task<IResult> SaveMessageAsync(ChatHistory<IChatUser> message);

        Task<Result<IEnumerable<ChatHistoryResponse>>> GetChatHistoryAsync(string userId, string contactId);
    }
}