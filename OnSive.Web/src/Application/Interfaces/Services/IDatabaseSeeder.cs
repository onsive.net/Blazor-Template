﻿namespace OnSive.Web.Application.Interfaces.Services
{
    public interface IDatabaseSeeder
    {
        void Initialize();
    }
}