﻿using System.Threading.Tasks;

namespace OnSive.Web.Application.Interfaces.Repositories
{
    public interface IProductRepository
    {
        Task<bool> IsBrandUsed(int brandId);
    }
}