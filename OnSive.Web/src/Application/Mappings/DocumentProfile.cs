﻿using AutoMapper;
using OnSive.Web.Application.Features.Documents.Commands.AddEdit;
using OnSive.Web.Application.Features.Documents.Queries.GetById;
using OnSive.Web.Domain.Entities.Misc;

namespace OnSive.Web.Application.Mappings
{
    public class DocumentProfile : Profile
    {
        public DocumentProfile()
        {
            CreateMap<AddEditDocumentCommand, Document>().ReverseMap();
            CreateMap<GetDocumentByIdResponse, Document>().ReverseMap();
        }
    }
}