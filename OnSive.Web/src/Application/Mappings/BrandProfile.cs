﻿using AutoMapper;
using OnSive.Web.Application.Features.Brands.Commands.AddEdit;
using OnSive.Web.Application.Features.Brands.Queries.GetAll;
using OnSive.Web.Application.Features.Brands.Queries.GetById;
using OnSive.Web.Domain.Entities.Catalog;

namespace OnSive.Web.Application.Mappings
{
    public class BrandProfile : Profile
    {
        public BrandProfile()
        {
            CreateMap<AddEditBrandCommand, Brand>().ReverseMap();
            CreateMap<GetBrandByIdResponse, Brand>().ReverseMap();
            CreateMap<GetAllBrandsResponse, Brand>().ReverseMap();
        }
    }
}