﻿using AutoMapper;
using OnSive.Web.Application.Features.DocumentTypes.Commands.AddEdit;
using OnSive.Web.Application.Features.DocumentTypes.Queries.GetAll;
using OnSive.Web.Application.Features.DocumentTypes.Queries.GetById;
using OnSive.Web.Domain.Entities.Misc;

namespace OnSive.Web.Application.Mappings
{
    public class DocumentTypeProfile : Profile
    {
        public DocumentTypeProfile()
        {
            CreateMap<AddEditDocumentTypeCommand, DocumentType>().ReverseMap();
            CreateMap<GetDocumentTypeByIdResponse, DocumentType>().ReverseMap();
            CreateMap<GetAllDocumentTypesResponse, DocumentType>().ReverseMap();
        }
    }
}