﻿using AutoMapper;
using OnSive.Web.Application.Features.Products.Commands.AddEdit;
using OnSive.Web.Domain.Entities.Catalog;

namespace OnSive.Web.Application.Mappings
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<AddEditProductCommand, Product>().ReverseMap();
        }
    }
}